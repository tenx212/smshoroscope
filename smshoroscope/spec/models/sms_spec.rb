require 'rails_helper'
require 'rubygems'

RSpec.describe Sms, type: :model do
    context 'Checks sms validation' do
    	it 'should be valid if sms contains only date in format dd-MM-yyyy' do
    		expect(Sms.sign_number('03-01-1990')).to be_truthy
    	end
    	it 'should be valid if sms contains users sign' do
    		expect(Sms.sign_number('cancer')).to be_truthy
    	end
    	it 'should be invalid if sms contains something else' do
    		expect(Sms.sign_number('some random text')).to be (false)
    	end
        it 'should return correct sign number to valid sms' do
            expect(Sms.sign_number('22-03-1990')).to eq 1
        end	
    end
end