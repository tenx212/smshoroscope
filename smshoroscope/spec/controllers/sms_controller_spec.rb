require 'rails_helper'
describe SmsController do
  context "Sms controller test" do
    it "should save received sms" do
      get :new_sms, {:sender => "37258131724", :message => "01-01-1990", :message_id => "123456"}
      expect(response.status).to eq 200
      expect(Sms.all).not_to be_empty
    end
    it "should return error if sms was invalid" do
      get :new_sms, {:sender => "37258131724", :message => "some invalid text", :message_id => "123456"}
      expect(response.status).to eq 600
    end   
  end
end