class CreateResponses < ActiveRecord::Migration
  def change
    create_table :responses do |t|
      t.references :sms, index: true, foreign_key: true
      t.string :text

      t.timestamps null: false
    end
  end
end
