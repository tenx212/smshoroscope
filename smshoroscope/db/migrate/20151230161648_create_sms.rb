class CreateSms < ActiveRecord::Migration
  def change
    create_table :sms do |t|
      t.string :text
      t.boolean :isValid

      t.timestamps null: false
    end
  end
end
