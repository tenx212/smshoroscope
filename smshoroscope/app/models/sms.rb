require 'time'
require 'zodiac'

class Sms < ActiveRecord::Base
	#Given text message, returns zodiac sign number if message is valid and false if message is invalid
	def self.sign_number (messageText)
		signNumbers = {"aries" => 1, "taurus" => 2, "gemini" => 3, "cancer" => 4, 
			"leo" => 5, "virgo" => 6, "libra" => 7, "scorpio" => 8, 
			"sagittarius" => 9, "capricorn" => 10, "aquarius" => 11, "pisces" => 12}
		begin
			if signNumbers.has_key?(messageText.downcase)
				signNumbers.fetch(messageText.downcase)
			else
				signNumbers.fetch(Time.parse(messageText).zodiac_sign.downcase)
			end
		rescue ArgumentError => message
			false
		end 
	end
end