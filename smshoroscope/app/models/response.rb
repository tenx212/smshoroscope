require 'rubygems'
require 'nokogiri'
require 'open-uri'

class Response < ActiveRecord::Base
  belongs_to :sms
  #Given zodiac sign number, returns weekly horoscope's first sentence from a website
  def self.getHoroscope(signNr)
  	url = "http://www.horoscope.com/us/horoscopes/general/horoscope-general-weekly.aspx?sign=" + signNr.to_s
  	page = Nokogiri::HTML(open(url))
  	page.css('div.block-horoscope-text')[0].text.gsub(/\s+/, ' ').split('.')[0]
  end
end

