class SmsController < ApplicationController
    def new_sms
        message = params[:message]
        #message is valid if sign number can be calculated and false is not returned
        validity = (!(Sms.sign_number(message)) == false)
        
        if validity
            @sms = Sms.create!({:text => message, :isValid => validity, :sign_number => Sms.sign_number(message)})
            @response = Response.create!({:sms => @sms, :text => Response.getHoroscope(@sms.sign_number)})
        	render :text => Response.getHoroscope(@sms.sign_number), :status => :ok
        else
            @sms = Sms.create!({:text => message, :isValid => validity})
        	render :text => "We could not read your sign! Please send us your zodiac sign or birth date in format dd-MM-yyyy", :status => 600
        end
    end
end